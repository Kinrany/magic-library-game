﻿using System;

public class DirectionConverter {

	public static AbsoluteDirection Turn(AbsoluteDirection absolute, RelativeDirection relative) {
		return (AbsoluteDirection)(((int)absolute + (int)relative) % (int)AbsoluteDirection.MAX);
	}

}

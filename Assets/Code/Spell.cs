﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Spell {
	public Spell(string word, ElementEnum element, TypeEnum type) {
		this.Word = word;
		this.Element = element;
		this.Type = type;
	}

	public readonly string Word;
	public readonly ElementEnum Element;
	public readonly TypeEnum Type;

	public const int MAX_LENGTH = 20;

	public static ElementEnum StrongerElement(ElementEnum element) {
		return (ElementEnum)(((int)element + 1) % (int)ElementEnum.COUNT);
	}
	public static ElementEnum WeakerElement(ElementEnum element) {
		return (ElementEnum)(((int)element + (int)ElementEnum.COUNT - 1) % (int)ElementEnum.COUNT);
	}

	public override string ToString() {
		return String.Format("{0} ({1} {2})", Word, Element, Type);
	}

	public enum ElementEnum {
		Storm,
		Earth,
		Fire,
		COUNT
	};

	public enum TypeEnum {
		Attack,
		Shield
	}
}

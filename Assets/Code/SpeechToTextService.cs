﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using AudioClip = UnityEngine.AudioClip;
using Application = UnityEngine.Application;

namespace SpeechToText {
	static class SpeechToTextService {
		public static void Analyze(Stream stream, ref string result, ref string error) {
			if (directory == null) {
				throw new Exception("Directory not set");
			}

			int fileNumber = CreateFileNumber();
			string name = "file" + fileNumber.ToString();
            string inputPath = Path.Combine(directory, inputPrefix + name + inputSuffix);
			string outputPath = Path.Combine(directory, outputPrefix + name + outputSuffix);

			FileStream fileStream = File.OpenWrite(inputPath);
			CopyStream(stream, fileStream);
			stream.Close();
			fileStream.Close();

			DateTime start = DateTime.UtcNow;
			while (!File.Exists(outputPath)) {
				Thread.Sleep(20);

				if ((DateTime.UtcNow - start).TotalSeconds > timeoutSeconds) {
					throw new TimeoutException();
				}
			}

			try {
				result = File.ReadAllText(outputPath);
				Debug.Assert(result != null);
				error = null;
				File.Delete(outputPath);
			}
			catch (Exception ex) {
				result = null;
				error = ex.ToString();
			}
		}

		public static void SetDirectory(string path) {
			directory = path;
		}

		static int fileCounter = 0;
		static System.Object lockObject = new System.Object();

		static string inputPrefix = "analyze-me-";
		static string inputSuffix = ".wav";
		static string outputPrefix = "result-";
		static string outputSuffix = ".txt";

		static string directory = null;

		static int timeoutSeconds = 20;
		
		static int CreateFileNumber() {
			lock (lockObject) {
				fileCounter += 1;
				return fileCounter;
			}
		}

		static void CopyStream(Stream input, Stream output) {
			byte[] buffer = new byte[32768];
			int read;
			while ((read = input.Read(buffer, 0, buffer.Length)) > 0) {
				output.Write(buffer, 0, read);
			}
		}
	}
}

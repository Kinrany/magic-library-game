﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

public static class Spells {
	public static Spell TryGet(string name) {
		if (dict == null) {
			throw new InvalidOperationException("List of spells has not been created yet, call Load() first.");
		}

		Spell spell = null;
		dict.TryGetValue(name, out spell);
		return spell;
	}

	public static Dictionary<string, Spell> Dict {
		get { return dict; }
	}

	public static void Load() {
		dict = new Dictionary<string, Spell>();
		Spell[] spells = new Spell[] {
			new Spell("YMLKFY", Spell.ElementEnum.Storm, Spell.TypeEnum.Shield),
			new Spell("KJUZCV", Spell.ElementEnum.Earth, Spell.TypeEnum.Shield),
			new Spell("FLKAJE", Spell.ElementEnum.Fire, Spell.TypeEnum.Shield),
			new Spell("KMAMEN", Spell.ElementEnum.Storm, Spell.TypeEnum.Attack),
			new Spell("AMVCIO", Spell.ElementEnum.Earth, Spell.TypeEnum.Attack),
			new Spell("EMDMLK", Spell.ElementEnum.Fire, Spell.TypeEnum.Attack)
		};
		foreach (var spell in spells) {
			dict[spell.Word] = spell;
		}
	}

	static Dictionary<string, Spell> dict = null;
}

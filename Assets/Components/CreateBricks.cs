﻿using UnityEngine;
using System;
using System.Collections;

public class CreateBricks : MonoBehaviour {

	public int BrickNumber;
	public GameObject BrickPrefab;

	// Use this for initialization
	void Start() {
		for (int i = 0; i < BrickNumber; ++i) {
			GameObject brick = (GameObject)Instantiate(BrickPrefab, transform);
			brick.transform.localPosition = new Vector3(0, i, 0);
			brick.name = String.Format("Brick {0}", i);
		}
	}
}

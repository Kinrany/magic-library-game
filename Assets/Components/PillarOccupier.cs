﻿using UnityEngine;
using System;
using System.Collections;

public class PillarOccupier : TileOccupier {

	public override bool Occupies(GridTile tile) {
		if (tile == null) {
			throw new ArgumentException("tile should not be null");
		}

		Debug.Assert(_movement != null);

		GridTile tile0 = _movement.Tile;
		if (tile0.Grid != tile.Grid) {
			return false;
		}

		for (int dx = -1; dx <= 1; ++dx) {
			for (int dy = -1; dy <= 1; ++dy) {
				if (tile0.X + dx == tile.X && tile0.Y + dy == tile.Y) {
					return true;
				}
			}
		}

		return false;
	}
}

﻿using UnityEngine;
using System.Collections;

public class SoundRecorder : MonoBehaviour {

	// Use this for initialization
	void Start() {
		deviceName = Microphone.devices[0];
		int _, frequency;
		Microphone.GetDeviceCaps(deviceName, out _, out frequency);
		clip = Microphone.Start(deviceName, false, 5, frequency);
	}

	// Update is called once per frame
	void Update() {
		if (flag && !Microphone.IsRecording(deviceName)) {
			flag = false;
			AudioSource.PlayClipAtPoint(clip, transform.position);
		}
	}

	void OnGUI() {
		if (!flag) {
			GUILayout.Label("YAY");
		}
	}

	bool flag = true;
	string deviceName;
	AudioClip clip;
}

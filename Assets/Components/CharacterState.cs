﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterState : MonoBehaviour {

	public GameObject EarthShield;
	public GameObject FireShield;
	public GameObject StormShield;
	public float StormRadius = 1;
	public float EarthRadius = 1.2f;
	public float FireRadius = 1.5f;
	public float ShieldAlpha = 0.3f;

	public CharacterState() : base() {
		shields = new Dictionary<Spell.ElementEnum, bool>();
	}

	public bool Alive {
		get {
			return alive;
		}
		set {
			alive = value;
		}
	}

	public void Reset() {
		SetShield(Spell.ElementEnum.Earth, false);
		SetShield(Spell.ElementEnum.Fire, false);
		SetShield(Spell.ElementEnum.Storm, false);

		Alive = true;
	}

	public void SetShield(Spell.ElementEnum element, bool enabled) {
		GameObject shield = null;
		switch (element) {
			case Spell.ElementEnum.Earth:
				shield = EarthShield;
				break;
			case Spell.ElementEnum.Fire:
				shield = FireShield;
				break;
			case Spell.ElementEnum.Storm:
				shield = StormShield;
				break;
			default:
				Debug.LogError("well crap, element is " + element.ToString());
				break;
		}
		shield.SetActive(enabled);
		shields[element] = enabled;
	}

	public bool GetShield(Spell.ElementEnum element) {
		return shields[element];
	}

	bool alive;
	readonly Dictionary<Spell.ElementEnum, bool> shields;

	void Start() {
		if (EarthShield == null || FireShield == null || StormShield == null) {
			Debug.LogError("Shield object not linked");
		}
		Reset();
	}
}

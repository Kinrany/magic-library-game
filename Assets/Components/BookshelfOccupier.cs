﻿using UnityEngine;
using System;
using System.Collections;

public class BookshelfOccupier : TileOccupier {

	public override bool Occupies(GridTile tile) {
		if (tile == null) {
			throw new ArgumentException("tile should not be null");
		}

		Debug.Assert(_movement != null);

		int x = _movement.Tile.X;
		int y = _movement.Tile.Y;
		GridTile tile0 = _movement.Tile;
		GridTile tile1, tile2;
		switch (_movement.Direction) {
			case AbsoluteDirection.East:
			case AbsoluteDirection.West:
				tile1 = _movement.Grid[x - 1, y];
				tile2 = _movement.Grid[x + 1, y];
				break;
			case AbsoluteDirection.North:
			case AbsoluteDirection.South:
				tile1 = _movement.Grid[x, y - 1];
				tile2 = _movement.Grid[x, y + 1];
				break;
			default:
				Debug.LogError("ow");
				return true;
		}

		return tile == tile0 || tile == tile1 || tile == tile2;
	}

}

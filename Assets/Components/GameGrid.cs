﻿using UnityEngine;
using System;
using System.Collections;

public class GameGrid : MonoBehaviour {

	// first coordinate is X, west -> east
	// second coordinate is Y, south -> north
	public readonly int Width = 30;
	public readonly int Length = 50;

	public GridTile TilePrefab;

	public GridTile GetSouth(GridTile tile) {
		for (int x = 0; x < Width; ++x) {
			for (int y = 0; y < Length; ++y) {
				if (grid[x, y] == tile) {
					return grid[x, y - 1];
				}
			}
		}
		return null;
	}
	public GridTile GetNorth(GridTile tile) {
		for (int x = 0; x < Width; ++x) {
			for (int y = 0; y < Length; ++y) {
				if (grid[x, y] == tile) {
					return grid[x, y + 1];
				}
			}
		}
		return null;
	}
	public GridTile GetEast(GridTile tile) {
		for (int x = 0; x < Width; ++x) {
			for (int y = 0; y < Length; ++y) {
				if (grid[x, y] == tile) {
					return grid[x + 1, y];
				}
			}
		}
		return null;
	}
	public GridTile GetWest(GridTile tile) {
		for (int x = 0; x < Width; ++x) {
			for (int y = 0; y < Length; ++y) {
				if (grid[x, y] == tile) {
					return grid[x - 1, y];
				}
			}
		}
		return null;
	}
	public GridTile GetNextTile(GridTile tile, AbsoluteDirection direction) {
		switch (direction) {
			case AbsoluteDirection.East:
				return GetEast(tile);
			case AbsoluteDirection.North:
				return GetNorth(tile);
			case AbsoluteDirection.South:
				return GetSouth(tile);
			case AbsoluteDirection.West:
				return GetWest(tile);
			default:
				throw new InvalidOperationException("Invalid direction.");
		}
	}

	public GridTile this[int x, int y] {
		get {
			if (x < 0 || x >= grid.GetLength(0) || y < 0 || y >= grid.GetLength(1)) {
				return null;
			}
			else {
				return grid[x, y];
			}
		}
	}
	
	void Awake() {
		grid = new GridTile[Width, Length];
		for (int x = 0; x < Width; ++x) {
			for (int y = 0; y < Length; ++y) {
				grid[x, y] = (GridTile)Instantiate(TilePrefab, transform);
				grid[x, y].Initialize(this, x, y);
				grid[x, y].name = String.Format("GridTile ({0}, {1})", x, y);
				grid[x, y].transform.position = new Vector3(GridTile.Width * x, 0, GridTile.Width * y);
			}
		}

		
	}

	// Update is called once per frame
	void Update() {

	}

	GridTile[,] grid;
}

public enum AbsoluteDirection {
	East,
	South,
	West,
	North,
	MAX,
	None
}

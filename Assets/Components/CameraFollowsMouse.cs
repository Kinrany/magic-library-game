﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowsMouse : MonoBehaviour {

	public float HorizontalSpeed = 3f;
	public float VerticalSpeed = 3f;
	public float MaxTilt = 40;
	public float MinTilt = -40;

	
	void Update () {
		float h = HorizontalSpeed * Input.GetAxis("Mouse X");
		float v = VerticalSpeed * Input.GetAxis("Mouse Y");
		transform.Rotate(Vector3.up, h, Space.World);
		transform.Rotate(-v, 0, 0);

		Vector3 euler = transform.rotation.eulerAngles;

		if (euler.x > 180) {
			euler.Set(euler.x - 360, euler.y, euler.z);
		}
		else if (euler.x < -180) {
			euler.Set(euler.x + 360, euler.y, euler.z);
		}

		if (euler.x > MaxTilt) {
			transform.rotation = Quaternion.Euler(MaxTilt, euler.y, euler.z);
		}
		else if (euler.x < MinTilt) {
			transform.rotation = Quaternion.Euler(MinTilt, euler.y, euler.z);
		}
	}
}

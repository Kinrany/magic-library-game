﻿using UnityEngine;
using System.Collections;

public class FireballBehavior : MonoBehaviour {

	void Start() {
		Foo();
	}

	// Update is called once per frame
	void Update() {
		var gridMovement = this.GetComponent<GridMovement>();
		
		try {
			if (!gridMovement.IsMoving) {
				gridMovement.StartMovementCoroutine(gridMovement.Direction);
			}
		}
		catch {
			Debug.LogError("Fireball reached the end of the map");
			Destroy(this.gameObject);
		}

		Foo();
	}

	void Foo() {
		var gridMovement = this.GetComponent<GridMovement>();
		GameObject other = TileOccupier.GetOccupier(gridMovement.Tile);
		if (other != null) {
			var burning = other.GetComponent<Burning>();
			if (burning != null) {
				burning.SetOnFire();
			}
			Destroy(this.gameObject);
		}
	}
}

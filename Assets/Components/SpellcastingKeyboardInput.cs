﻿using UnityEngine;
using System.Collections;

public class SpellcastingKeyboardInput : MonoBehaviour {

	// Use this for initialization
	void Start() {
		spellcasting = gameObject.GetComponent<Spellcasting>();
	}

	// Update is called once per frame
	void Update() {
		for (KeyCode key = KeyCode.A; key <= KeyCode.Z; ++key) {
			if (Input.GetKeyDown(key)) {
				spellcasting.Add(key.ToString());
			}
		}
	}

	Spellcasting spellcasting = null;
}

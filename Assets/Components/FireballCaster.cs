﻿using UnityEngine;
using System.Collections;
using Microsoft.Azure.Engagement.Unity;

public class FireballCaster : MonoBehaviour {

	public GridMovement FireballPrefab;

	public void Cast() {
		var gridMovement = this.GetComponent<GridMovement>();
		var fireball = Instantiate(FireballPrefab);
		fireball.Tile = gridMovement.GetNextTile(gridMovement.Direction);
		fireball.Direction = gridMovement.Direction;
		EngagementAgent.SendEvent("Spell: Fireball");
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.F)) {
			Cast();
		}
	}
}

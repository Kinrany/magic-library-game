﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Spellcasting : MonoBehaviour {

	public GameObject Target;

	public string CurrentSpell {
		get {
			return currentSpell;
		}
		private set {
			currentSpell = value;

			for (int i = 0; i < currentSpell.Length; ++i) {
				string substr = currentSpell.Substring(i);
				Spell spell = Spells.TryGet(substr);
				if (spell != null) {
					CastSpell(spell);
					currentSpell = "";
					break;
				}
			}

			if (currentSpell.Length > Spell.MAX_LENGTH) {
				currentSpell = currentSpell.Substring(currentSpell.Length - Spell.MAX_LENGTH);
			}
		}
	}

	public void Add(string str) {
		CurrentSpell += str;
	}
	public void Add(char chr) {
		CurrentSpell += chr;
	}
	
	public void Reset() {
		currentSpell = "";
	}

	public event Action<Spell> AfterSpellCastEvent;


	void CastSpell(Spell spell) {
		Debug.Log(gameObject.name + " casts spell " + spell.ToString());

		switch (spell.Type) {
			case Spell.TypeEnum.Shield:
				gameObject.GetComponent<CharacterState>().SetShield(spell.Element, true);
				break;
			case Spell.TypeEnum.Attack:
				var character = Target.GetComponent<CharacterState>();
				if (character.GetShield(spell.Element) == true) {
					character.SetShield(spell.Element, false);
				}
				else if (character.GetShield(Spell.StrongerElement(spell.Element)) == false) {
					character.Alive = false;
				}
				break;
		}

		AfterSpellCastEvent(spell);
	}

	string currentSpell = "";
}

﻿using UnityEngine;
using System.Collections;

public class BrickHeight : MonoBehaviour {

	public int Height;

	public void SetHeight(int height) {
		var pos = transform.position;
		transform.position = new Vector3(pos.x, height, pos.z);
	}

	// Use this for initialization
	void Start() {
		SetHeight(Height);
	}

	// Update is called once per frame
	void Update() {

	}
}

﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameMap : MonoBehaviour {

	public GridMovement BookshelfPrefab;
	public GridMovement ChandelierPrefab;
	public GridMovement PillarPrefab;
	public GridMovement WallPrefab;

	GameGrid _grid;

	// Use this for initialization
	void Start() {
		_grid = GetComponent<GameGrid>();

		// number of shelves on short and long side of the room
		int shelfWidth = 3;
		int shelfLength = 5;

		// number of tiles used by the room
		int width = 2 + 3 * shelfWidth + 3 * (shelfWidth + 1);
		int length = 2 + 3 * shelfLength + 3 * (shelfLength + 1);

		Debug.Assert(_grid.Width > width);
		Debug.Assert(_grid.Length > length);

		// create walls, starting with (1, 1) and finishing with (width, length)
		for (int x = 1; x <= width; ++x) {
			AddWall(x, 1);
			AddWall(x, length);
		}
		for (int y = 2; y <= length - 1; ++y) {
			AddWall(1, y);
			AddWall(width, y);
		}

		// create pillars, short sides of the room first
		for (int i = 1; i <= shelfWidth + 1; ++i) {
			int x = -3 + i * 6;
			AddPillar(x, 3);
			AddPillar(x, length - 2);
		}
		// pillars on the corners of the room are not created twice
		for (int i = 2; i <= shelfLength; ++i) {
			int y = -3 + i * 6;
			AddPillar(3, y);
			AddPillar(width - 2, y);
		}

		// create shelves along the short sides
		for (int i = 1; i <= shelfWidth; ++i) {
			int x = 0 + i * 6;
			AddBookshelf(x, 3, AbsoluteDirection.East);
			AddBookshelf(x, length - 2, AbsoluteDirection.West);
		}
		// create shelves and chandeliers along the long sides
		for (int i = 1; i <= shelfLength; ++i) {
			int y = 0 + i * 6;
			AddBookshelf(3, y, AbsoluteDirection.South);
			AddChandelier(3 + 3, y);
			AddBookshelf(width - 2, y, AbsoluteDirection.North);
			AddChandelier(width - 2 - 3, y);
		}

	}

	void AddBookshelf(int x, int y, AbsoluteDirection direction) {
		GridMovement bookshelf = (GridMovement)Instantiate(BookshelfPrefab, transform);
		bookshelf.name = string.Format("Bookshelf ({0}, {1})", x, y);
		bookshelf.Tile = _grid[x, y];
		bookshelf.Direction = direction;
	}
	void AddChandelier(int x, int y) {
		GridMovement chandelier = (GridMovement)Instantiate(ChandelierPrefab, transform);
		chandelier.name = string.Format("Chandelier ({0}, {1})", x, y);
		chandelier.Tile = _grid[x, y];
	}
	void AddPillar(int x, int y) {
		GridMovement pillar = (GridMovement)Instantiate(PillarPrefab, transform);
		pillar.name = string.Format("Pillar ({0}, {1})", x, y);
		pillar.Tile = _grid[x, y];
	}
	void AddWall(int x, int y) {
		GridMovement wall = (GridMovement)Instantiate(WallPrefab, transform);
		wall.name = string.Format("Wall ({0}, {1})", x, y);
		wall.Tile = _grid[x, y];
	}
}

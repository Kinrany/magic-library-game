﻿using UnityEngine;
using System.Collections;

public class GridPosition : MonoBehaviour {
	
	public GridPosition() : base() {
		Grid = null;
		Direction = new AbsoluteDirection();
	}

	public GridTile Tile;
	public AbsoluteDirection Direction;

	public GameGrid Grid {
		get {
			return _grid;
		}
		private set {
			_grid = value;
		}
	}

	public void SetGrid(GameGrid grid) {
		Grid = grid;
	}

	GameGrid _grid;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Azure.Engagement.Unity;

public class FollowCamera : MonoBehaviour {

	public Camera Camera;

	// Use this for initialization
	void Start() {
		_movement = GetComponent<GridMovement>();
	}

	// Update is called once per frame
	void Update() {
		var eulerY = Camera.transform.rotation.eulerAngles.y;
		while (eulerY < 45) {
			eulerY += 360;
		}
		while (eulerY > 45 + 360) {
			eulerY -= 360;
		}

		AbsoluteDirection oldDirection = _movement.Direction;
		if (45 <= eulerY && eulerY <= 45 + 90) {
			_movement.Direction = AbsoluteDirection.East;
		}
		else if (45 + 90 <= eulerY && eulerY <= 45 + 180) {
			_movement.Direction = AbsoluteDirection.South;
		} 
		else if (45 + 180 <= eulerY && eulerY <= 45 + 270) {
			_movement.Direction = AbsoluteDirection.West;
		}
		else if (45 + 270 <= eulerY && eulerY <= 45 + 360) {
			_movement.Direction = AbsoluteDirection.North;
		}

		// fake spell events
		//
		//if (_movement.Direction != oldDirection) {

		//	string eventText = "Spell: ";
		//	switch (_movement.Direction) {
		//		case AbsoluteDirection.East:
		//			eventText += "Fireball";
		//			break;
		//		case AbsoluteDirection.West:
		//			eventText += "Steal book";
		//			break;
		//		case AbsoluteDirection.North:
		//			eventText += "Telekinesis";
		//			break;
		//		case AbsoluteDirection.South:
		//			eventText += "Teleportation";
		//			break;
		//	}

		//	EngagementAgent.SendEvent(eventText);		
		//}	
	}

	GridMovement _movement;
}

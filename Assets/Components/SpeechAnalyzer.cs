﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using UnityEngine;
using SpeechToText;

public class SpeechAnalyzer : MonoBehaviour {

	public FireballCaster FireballCaster = null;

	void Start() {
		SpeechToTextService.SetDirectory(Application.persistentDataPath);
		Debug.Log("Path: " + Application.persistentDataPath);

		if (Microphone.devices.Length == 0) {
			error = "Failed to locate microphone.";
            state = State.Error;
		}
		else {
			deviceName = Microphone.devices[0];
		}
	}

	void OnGUI() {
		GUILayout.Label(state.ToString());
		switch (state) {
			case State.Idle:
			case State.Finished:
				bool start = GUILayout.Button("Press to start recording");
				if (start) {
					StartCoroutine(Recording());
				}
				if (result != null) {
					GUILayout.Label("Result: " + result);
				}
				break;
			case State.Recording:
				GUILayout.Button("Recording...");
				break;
			case State.Analyzing:
				GUILayout.Button("Analyzing...");
				break;
			case State.Error:
				GUILayout.Button("Error: " + error);
				break;
		}
	}

	IEnumerator Recording() {
		Debug.Assert(state == State.Idle || state == State.Finished);

		state = State.Recording;

		AudioClip clip = Microphone.Start(deviceName, false, clipLength, frequency);
		while (Microphone.IsRecording(deviceName)) {
			yield return wait200;
		}
		MemoryStream stream = new MemoryStream();
		SavWav.SaveStream(stream, clip);

		state = State.Analyzing;

		Thread thread = new Thread(() => {
			try {
				SpeechToTextService.Analyze(stream, ref result, ref error);
			}
			catch (Exception e) {
				result = null;
				error = e.ToString();
			}
		});

		result = null;
		error = null;
		thread.Start();
		while (result == null && error == null) {
			yield return wait200;
		}

		state = (error == null) ? State.Finished : State.Error;
		if (state == State.Finished) {
			ProcessPhrase(result);
		}
	}

	State state = State.Idle;
	string result = null;
	string error = null;

	string deviceName;
	int clipLength = 3;
	int frequency = 36000;

	WaitForSeconds wait200 = new WaitForSeconds(0.200f);

	void ProcessPhrase(string phrase) {
		if (FireballCaster != null && phrase.Contains("fire")) {
			FireballCaster.Cast();
		}
	}

	enum State {
		Idle,
		Recording,
		Analyzing,
		Finished,
		Error
	}
}

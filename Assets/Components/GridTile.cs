﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridTile : MonoBehaviour {

	public GameGrid Grid {
		get {
			Debug.Assert(initialized);
			return _grid;
		}
	}
	public int X {
		get {
			Debug.Assert(initialized);
			return _x;
		}
	}
	public int Y {
		get {
			Debug.Assert(initialized);
			return _y;
		}
	}

	public static float Width = 1.3f;

	public void Initialize(GameGrid grid, int x, int y) {
		Debug.Assert(!initialized);
		initialized = true;

		_grid = grid;
		_x = x;
		_y = y;
	}

	bool initialized = false;
	GameGrid _grid;
	int _x, _y;
}

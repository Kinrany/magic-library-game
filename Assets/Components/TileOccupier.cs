﻿using UnityEngine;
using System.Collections;

public class TileOccupier : MonoBehaviour {

	public bool Occupies(int x, int y) {
		GridTile tile = _movement.Grid[x, y];
		return Occupies(tile);
	}
	public virtual bool Occupies(GridTile tile) {
		return _movement.Tile == tile;
	}

	public static bool IsOccupied(GameGrid grid, int x, int y) {
		GridTile tile = grid[x, y];
		return IsOccupied(tile);
	}
	public static bool IsOccupied(GridTile tile) {
		return GetOccupier(tile) != null;
	}
	public static GameObject GetOccupier(GameGrid grid, int x, int y) {
		GridTile tile = grid[x, y];
		return GetOccupier(tile);
	}
	public static GameObject GetOccupier(GridTile tile) {
		var occupiers = FindObjectsOfType<TileOccupier>();
		foreach (var occupier in occupiers) {
			if (occupier.Occupies(tile)) {
				return occupier.gameObject;
			}
		}
		return null;
	}

	// Use this for initialization
	protected void Start () {
		_movement = GetComponent<GridMovement>();
		Debug.Assert(_movement != null);
	}

	protected GridMovement _movement = null;
}

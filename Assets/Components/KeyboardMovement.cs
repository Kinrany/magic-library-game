﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Azure.Engagement.Unity;

public class KeyboardMovement : MonoBehaviour {

	GridMovement _movement;

	float _ghostMovementTime = -1;
	AbsoluteDirection _ghostMovementDirection = AbsoluteDirection.None;
	float _ghostMovementTimeout = Mathf.Min(0.1f, GridMovement.MovementCoroutineSeconds);

	// Use this for initialization
	void Start() {
		_movement = gameObject.GetComponent<GridMovement>();
	}

	// Update is called once per frame
	void Update() {
		RelativeDirection relative = RelativeDirection.None;

		if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
			relative = RelativeDirection.Forward;
		}
		else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
			relative = RelativeDirection.Right;
		}
		else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
			relative = RelativeDirection.Backwards;
		}
		else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
			relative = RelativeDirection.Left;
		}

		AbsoluteDirection absolute = AbsoluteDirection.None;
		if (!_movement.IsMoving) {
			if (relative != RelativeDirection.None) {
				absolute = DirectionConverter.Turn(_movement.Direction, relative);
			}
			else if (_ghostMovementTimeout > Time.time - _ghostMovementTime) {
				absolute = _ghostMovementDirection;
			}

			if (absolute != AbsoluteDirection.None) {
				MoveInDirection(absolute);
			}
		}
		else {
			if (relative != RelativeDirection.None) {
				_ghostMovementTime = Time.time;
				_ghostMovementDirection = DirectionConverter.Turn(_movement.Direction, relative);
			}
		}
		
	}

	void MoveInDirection(AbsoluteDirection direction) {
		GridTile tile = _movement.GetNextTile(direction);
		if (!TileOccupier.IsOccupied(tile)) {
			_movement.StartMovementCoroutine(direction);

			string eventText = "Spell: ";
			switch (_movement.Direction) {
				case AbsoluteDirection.East:
					eventText += "Fireball";
					break;
				case AbsoluteDirection.West:
					eventText += "Steal book";
					break;
				case AbsoluteDirection.North:
					eventText += "Telekinesis";
					break;
				case AbsoluteDirection.South:
					eventText += "Teleportation";
					break;
			}
			EngagementAgent.SendEvent(eventText);
			Debug.Log("Player moved");
		}
	}
}

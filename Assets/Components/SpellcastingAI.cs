﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SpellcastingAI : MonoBehaviour {

	public float TypingChance = 0.1f;
	public float ErrorChance = 0.1f;

	void Start() {
		spellcasting = gameObject.GetComponent<Spellcasting>();
		character = gameObject.GetComponent<CharacterState>();

		spellcasting.AfterSpellCastEvent += (_) => currentSpell = "";
	}
	
	void FixedUpdate() {
		if (!character.Alive) {
			return;
		}

		if (currentSpell == "") {
			var spellCount = Spells.Dict.Count;
			var spellNumber = Random.Range(0, spellCount);
			var spell = Spells.Dict.Skip(spellNumber).First().Value;
			currentSpell = spell.Word;
		}

		if (Random.Range(0, 1.0f) < TypingChance) {
			if (Random.Range(0, 1.0f) < ErrorChance) {
				char chr = (char)Random.Range('A', 'Z' + 1);
				spellcasting.Add(chr);
				currentSpell = "";
			}
			else {
				char chr = currentSpell[0];
				currentSpell = currentSpell.Substring(1);
				spellcasting.Add(chr);
			}
		}
	}

	string currentSpell = "";
	Spellcasting spellcasting = null;
	CharacterState character = null;
}

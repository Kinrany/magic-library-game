﻿using UnityEngine;
using System;
using System.Collections;

public class GridMovement : MonoBehaviour {

	public int START_X = 0;
	public int START_Y = 0;
	public AbsoluteDirection START_DIRECTION = AbsoluteDirection.East;
	public GameGrid StartGrid = null;
	public static float MovementCoroutineSeconds = .3f;

	public GameGrid Grid {
		get {
			if (Tile == null) {
				return null;
			}
			else {
				return Tile.Grid;
			}
		}
	}
	public GridTile Tile {
		get {
			return _tile;
		}
		set {
			_tile = value;
			UpdateTransformToTile();
		}
	}
	public AbsoluteDirection Direction {
		get {
			return _direction;
		}
		set {
			_direction = value;
			UpdateTransformToDirection();
		}
	}
	public bool IsMoving {
		get {
			return _isMoving;
		}
	}

	public void MoveForward() {
		MoveToNextTile(RelativeDirection.Forward);
	}
	public void MoveRight() {
		MoveToNextTile(RelativeDirection.Right);
	}
	public void MoveBackwards() {
		MoveToNextTile(RelativeDirection.Backwards);
	}
	public void MoveLeft() {
		MoveToNextTile(RelativeDirection.Left);
	}
	public GridTile GetNextTile(RelativeDirection relativeDirection) {
		return GetNextTile(DirectionConverter.Turn(Direction, relativeDirection));
	}
	public GridTile GetNextTile(AbsoluteDirection direction) {
		return Grid.GetNextTile(Tile, direction);
	}
	public void MoveToNextTile(RelativeDirection relativeDirection) {
		// transition to new tile?
		Tile = GetNextTile(relativeDirection);
	}

	public void StartMovementCoroutine(RelativeDirection relative) {
		AbsoluteDirection absolute = DirectionConverter.Turn(Direction, relative);
		StartMovementCoroutine(absolute);
	}
	public void StartMovementCoroutine(AbsoluteDirection absolute) {
		StartCoroutine(MovementCoroutine(absolute));
	}

	
	GridTile _tile = null;
	AbsoluteDirection _direction = new AbsoluteDirection();
	bool _isMoving = false;

	void Start() {
		if (StartGrid != null) {
			Tile = StartGrid[START_X, START_Y];
			Direction = START_DIRECTION;
		}
	}

	void UpdateTransformToTile() {
		transform.position = UpdatedPosition();
	}
	void UpdateTransformToDirection() {
		float x = transform.rotation.eulerAngles.x;
		float y = (int)Direction * 90 - 90;
		float z = transform.rotation.eulerAngles.z;
		transform.rotation = Quaternion.Euler(x, y, z);
	}
	Vector3 UpdatedPosition() {
		Vector3 tilePos = Tile.transform.position;
		return new Vector3(tilePos.x, transform.position.y, tilePos.z);
	}
	IEnumerator MovementCoroutine(AbsoluteDirection direction) {
		Debug.Assert(!_isMoving);
		_isMoving = true;

		Vector3 start = transform.position;
		Tile = GetNextTile(direction);
		Vector3 end = UpdatedPosition();
		
		float seconds = .3f;
		float startTime = Time.time;
		while (Time.time < startTime + seconds) {
			float progress = (Time.time - startTime) / seconds;
			transform.position = Vector3.Lerp(start, end, progress);
			yield return null;
		}

		_isMoving = false;
	}
}

public enum RelativeDirection {
	Forward,
	Right,
	Backwards,
	Left,
	MAX,
	None
}

﻿using UnityEngine;
using System.Collections;

public class Burning : MonoBehaviour {

	public GameObject[] Fires;

	public void SetOnFire() {
		if (_coroutine != null) {
			StopCoroutine(_coroutine);
		}
		_coroutine = StartCoroutine(BurningCoroutine());
	}

	public bool IsBurning {
		get {
			return _burning;
		}
		private set {
			_burning = value;
			foreach (var fire in Fires) {
				fire.SetActive(_burning);
			}
		}
	}

	private bool _burning = false;
	private Coroutine _coroutine = null;

	private IEnumerator BurningCoroutine() {
		IsBurning = true;
		yield return new WaitForSeconds(4);
		IsBurning = false;
		_coroutine = null;
	}
}

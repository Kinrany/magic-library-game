﻿using UnityEngine;
using System.Collections;

public class CameraFollowsBody : MonoBehaviour {

	public Transform Body;

	// Use this for initialization
	void Awake() {
		_relativePosition = transform.position - Body.transform.position;
	}

	// Update is called once per frame
	void OnPreCull() {
		transform.position = Body.transform.position + _relativePosition;
	}

	Vector3 _relativePosition;
}

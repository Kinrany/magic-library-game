﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class DebugGUI : MonoBehaviour {
	public GameObject WizardObject;
	public GameObject EnemyObject;
	public float Width = 400;
	public float SpellWidth = 300;

	void Start() {
		wizardSpellcasting = WizardObject.GetComponent<Spellcasting>();
		enemySpellcasting = EnemyObject.GetComponent<Spellcasting>();
		wizardCharacter = WizardObject.GetComponent<CharacterState>();
		enemyCharacter = EnemyObject.GetComponent<CharacterState>();

		wizardSpellcasting.AfterSpellCastEvent += (spell) => {
			lastSpell = spell.Word;
		};
		enemySpellcasting.AfterSpellCastEvent += (spell) => {
			enemyLastSpell = spell.Word;
		};
	}

	void OnGUI() {
		GUIStyle boxStyle = new GUIStyle("Box");

		// screen
		GUILayout.BeginVertical();
		{
			// players
			GUILayout.BeginHorizontal();
			{
				// wizard
				GUILayout.BeginVertical(boxStyle);
				{
					GUILayout.Label("Wizard:");

					// current spell
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Current spell: ");
						GUILayout.TextField(wizardSpellcasting.CurrentSpell, GUILayout.Width(SpellWidth));
					}
					GUILayout.EndHorizontal();

					// previous spell
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Last spell: ");
						GUILayout.TextField(lastSpell, GUILayout.Width(SpellWidth));
					}
					GUILayout.EndHorizontal();

					// alive
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Alive: ");
						GUILayout.Label(wizardCharacter.Alive.ToString());
					}
					GUILayout.EndHorizontal();
				}
				GUILayout.EndVertical();

				// enemy
				GUILayout.BeginVertical(boxStyle);
				{
					GUILayout.Label("Enemy:");

					// current spell
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Current spell: ");
						GUILayout.TextField(enemySpellcasting.CurrentSpell, GUILayout.Width(SpellWidth));
					}
					GUILayout.EndHorizontal();

					// previous spell
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Last spell: ");
						GUILayout.TextField(enemyLastSpell, GUILayout.Width(SpellWidth));
					}
					GUILayout.EndHorizontal();

					// alive
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("Alive: ");
						GUILayout.Label(enemyCharacter.Alive.ToString());
					}
					GUILayout.EndHorizontal();
				}
				GUILayout.EndVertical();
			}
			GUILayout.EndHorizontal();

			// spellbook
			GUILayout.BeginVertical(boxStyle);
			{
				GUILayout.Label("Spellbook:");
				
				// columns
				GUILayout.BeginHorizontal();
				{
					// column 1
					GUILayout.BeginVertical(GUILayout.Width(160));
					{
						foreach (var pair in Spells.Dict.Take(3)) {
							var spell = pair.Value;
							bool pressed = GUILayout.Button(spell.ToString());

							if (pressed) {
								wizardSpellcasting.Add(spell.Word);
							}
						}
					}
					GUILayout.EndVertical();

					// column 2
					GUILayout.BeginVertical(GUILayout.Width(160));
					{
						foreach (var pair in Spells.Dict.Skip(3).Take(3)) {
							var spell = pair.Value;
							bool pressed = GUILayout.Button(spell.ToString());

							if (pressed) {
								wizardSpellcasting.Add(spell.Word);
							}
						}
					}
					GUILayout.EndVertical();
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();

			// reset button
			bool clicked = GUILayout.Button("Reset");

			if (clicked) {
				wizardCharacter.Reset();
				enemyCharacter.Reset();
			}
		}
		GUILayout.EndVertical();
	}

	Spellcasting wizardSpellcasting;
	Spellcasting enemySpellcasting;
	CharacterState wizardCharacter;
	CharacterState enemyCharacter;

	string lastSpell = "";
	string enemyLastSpell = "";
}
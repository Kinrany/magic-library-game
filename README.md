## README

Файлы с моделями, а также любые другие файлы, занимающие много места и не требующие контроля версий должны распространяться отдельно.

По этой причине содержимое Assets/Models игрорируется.